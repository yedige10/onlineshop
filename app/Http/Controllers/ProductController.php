<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use Validator;
use Illuminate\Support\Facades\Auth;
use File;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Support\Facades\Storage;
class ProductController extends Controller
{
    public function createProduct(Request $request){

        $validator =  Validator::make($request->all(),[
            'product.name'=>'required',
            'product.category_id'=>'required',
            'product.count'=>'required',
            'product.price'=>'required',
            'product.discount'=>'required',
            'product.discountprice'=>'required',
            'product.images'=>'required|array|min:1'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'error' => true,
                'message' =>'Заполните все поля и выберите фото товара',
                'data' => [
                ]
            ], 200);
        }
        $product=$request->product;
        $created_product=Product::create([
            'name'=>$product['name'],
            'count'=>$product['count'],
            'price'=>$product['price'],
            'discount'=>$product['discount'],
            'price_discount'=>$product['discountprice'],
            'category_id'=>$product['category_id']
        ]);
        $images=$product['images'];
        for($i=0;$i<sizeof($images);$i++){
            $exploded=explode(',',$images[$i]);
            $decoded=base64_decode($exploded[1]);
            if(str_contains($exploded[0],'jpeg')){
                $extension='jpg';
            }
            else{
                $extension='png';
            }
            $filename=str_random().'.'.$extension;
            $path=public_path().'/images/'.$filename;
            file_put_contents($path,$decoded);
            $productImage=ProductImage::create([
                'product_id'=>$created_product['id'],
                'image'=>$filename
            ]);
        }
        return response()->json([
            'success' => true,
            'error' => false,
            'message' => 'Successfully created!',
            'data' => [
            ]
        ], 200);
    }

    public function editProduct(Request $request){

        $validator =  Validator::make($request->all(),[
            'product.name'=>'required',
            'product.category_id'=>'required',
            'product.count'=>'required',
            'product.price'=>'required',
            'product.discount'=>'required',
            'product.discountprice'=>'required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'error' => true,
                'message' =>'Заполните все поля и выберите фото товара',
                'data' => [
                ]
            ], 200);
        }
        $product=$request->product;
        
        Product::where('id',$product['id'])->update([
            'name'=>$product['name'],
            'count'=>$product['count'],
            'price'=>$product['price'],
            'discount'=>$product['discount'],
            'price_discount'=>$product['discountprice'],
            'category_id'=>$product['category_id']
        ]);
        $images=ProductImage::where('product_id',$product['id'])->pluck('id')->toArray();
        
        $productImages=array();
        foreach($product['images'] as $productImage){
            array_push($productImages,$productImage['id']);
        }
        foreach($images as $image){
            if(!in_array($image,$productImages)){
                ProductImage::where('id',$image)->delete();
                File::delete(public_path('images/'.$image));
            }
        }
        $addedImages=$product['addedImages'];
        for($i=0;$i<sizeof($addedImages);$i++){
            $exploded=explode(',',$addedImages[$i]);
            $decoded=base64_decode($exploded[1]);
            if(str_contains($exploded[0],'jpeg')){
                $extension='jpg';
            }
            else{
                $extension='png';
            }
            $filename=str_random().'.'.$extension;
            $path=public_path().'/images/'.$filename;
            file_put_contents($path,$decoded);
            $productImage=ProductImage::create([
                'product_id'=>$product['id'],
                'image'=>$filename
            ]);
        }
        return response()->json([
            'success' => true,
            'error' => false,
            'message' => 'Successfully created!',
            'data' => [
            ]
        ], 200);
    }
    
    public function getProductsByCategory($id){
        $products=Product::select('product.*','category.name as category')->
            join('category','category.id','=','product.category_id')->
            where('category_id',$id)->get();
        
        foreach($products as $product){
            $images=ProductImage::where('product_id',$product['id'])->pluck('image');
            $product['images']=$images;
        }    
        return response()->json([
            'success' => true,
            'error' => false,
            'message' => 'Successfully created!',
            'data' => [
                'products'=>$products
            ]
        ], 200);
    }

    public function getProduct($id){
        $product=Product::where('id',$id)->first();
        $product['images']=ProductImage::where('product_id',$product['id'])->get();
        return response()->json([
            'success' => true,
            'error' => false,
            'message' => 'Successfully created!',
            'data' => [
                'product'=>$product
            ]
        ], 200);
    }

    public function updateCategory(Request $request){
        $category=$request->category;
        
        $validator =  Validator::make($request->all(),[
            'category.name'=>'required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'error' => true,
                'message' =>'Заполните категорию',
                'data' => [
                ]
            ], 200);
        }
        $validator =  Validator::make($request->all(),[
            'category.name'=>'unique:App\Models\Category,name'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'error' => true,
                'message' =>'Категория уже существует',
                'data' => [
                ]
            ], 200);
        }
        
        Category::where('id',$category['id'])->update([
            'name'=>$category['name']
        ]);
        return response()->json([
            'success' => true,
            'error' => false,
            'message' => 'Successfully created!',
            'data' => [
            ]
        ], 200);

    }

    public function deleteProduct(Request $request){
        $product=$request->product;
        $images=ProductImage::where('product_id',$product['id'])->get();
        foreach($images as $image){
            File::delete(public_path('images/'.$image['image'])); 
        }
        ProductImage::where('product_id',$product['id'])->delete();
        Product::where('id',$product['id'])->delete();
        return response()->json([
            'success' => true,
            'error' => false,
            'message' => 'Товар успешно удален',
            'data' => [
            ]
        ], 200);
    }
}
