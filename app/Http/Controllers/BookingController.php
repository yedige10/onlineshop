<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use App\Models\OrderBooking;
use App\Models\OrderBookingProduct;
use Validator;
class BookingController extends Controller
{
    public function getBookings(){
        $booking=OrderBooking::all();
        foreach($booking as $book){
            $book['products']=OrderBookingProduct::select('orderbookingproduct.*','product.name as product','category.name as category')->
                join('product','product.id','=','orderbookingproduct.product_id')->
                join('category','category.id','=','product.category_id')->
                where('orderbookingproduct.order_booking_id',$book['id'])->get();
        }
        return response()->json([
            'success' => true,
            'error' => false,
            'message' => 'Successfully created!',
            'data' => [
                'booking'=>$booking
            ]
        ], 200);
    }
    
}
