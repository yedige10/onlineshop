<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainWebRouteController extends Controller
{
    //
    public function loadAdmin()
    {
    	# code...
    	return view('admin');
    }

    public function loadEditor()
    {
    	# code...
    	return view('editor');
    }

    public function loadModerator()
    {
        # code...
        return view('moderator');
    }

    public function loadUser()
    {
        # code...
        return view('welcome');
    }

    public function loadTeacher(){
        return view('teacher');
    }
}
