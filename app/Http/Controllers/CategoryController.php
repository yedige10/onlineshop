<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use Validator;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductImage;

class CategoryController extends Controller
{
    public function createCategory(Request $request){

        $validator =  Validator::make($request->all(),[
            'category'=>'required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'error' => true,
                'message' =>'Заполните категорию',
                'data' => [
                ]
            ], 200);
        }
        $validator =  Validator::make($request->all(),[
            'category'=>'unique:App\Models\Category,name'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'error' => true,
                'message' =>'Категория уже существует',
                'data' => [
                ]
            ], 200);
        }
        
        
        Category::create([
            'name'=>$request->category,
        ]);
        
        return response()->json([
            'success' => true,
            'error' => false,
            'message' => 'Successfully created!',
            'data' => [
                'name'=>$request->category
            ]
        ], 200);
    }
    
    public function getCategories(){
        return response()->json([
            'success' => true,
            'error' => false,
            'message' => 'Successfully created!',
            'data' => [
                'categories'=>Category::all()
            ]
        ], 200);
    }


    public function updateCategory(Request $request){
        $category=$request->category;
        
        $validator =  Validator::make($request->all(),[
            'category.name'=>'required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'error' => true,
                'message' =>'Заполните категорию',
                'data' => [
                ]
            ], 200);
        }
        $validator =  Validator::make($request->all(),[
            'category.name'=>'unique:App\Models\Category,name'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'error' => true,
                'message' =>'Категория уже существует',
                'data' => [
                ]
            ], 200);
        }
        
        Category::where('id',$category['id'])->update([
            'name'=>$category['name']
        ]);
        return response()->json([
            'success' => true,
            'error' => false,
            'message' => 'Successfully created!',
            'data' => [
            ]
        ], 200);

    }

    public function deleteCategory(Request $request){
        $category=$request->category;
        $products=Product::where('category_id',$category['id'])->get();
        foreach($products as $product){
            ProductImage::where('product_id',$product['id'])->delete();
        }
        Product::where('category_id',$category['id'])->delete();
        Category::where('id',$category['id'])->delete();
        return response()->json([
            'success' => true,
            'error' => false,
            'message' => 'Категория успешно удалено',
            'data' => [
            ]
        ], 200);
    }
}
