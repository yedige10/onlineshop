<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use Illuminate\Support\Facades\Auth;
use App\Models\SentLetter;
use Illuminate\Support\Facades\Config;
use App\Mail\MessageMail;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use App\Models\OrderBooking;
use App\Models\OrderBookingProduct;
use Validator;
class EmployeeController extends Controller
{
    public function register(Request $request){
        Employee::create([
            'username'=>'yedige10',
            'role'=>'admin',
            'password'=>bcrypt('ktl1997')
        ]);
        return response()->json([
            'success' => true,
            'error' => false,
            'message' => 'Successfully registered!',
            'data' => [
            ]
        ], 200);
    }


    public function login(Request $request){
        $user=$request->user;
         
        if(Auth::guard('employee')->attempt(['username' => $user['username'], 'password' => $user['password']])){
            // getting user
            $emp = Employee::where('username',  $user['username'])->first(); //Auth::user();
            $objToken = $emp->createToken('API ITgate - '.$emp['role']);
            $token = $objToken->accessToken;
            $expiration = $objToken->token->expires_at->diffInSeconds(Carbon::now()->addHours(24));
            $access = Crypt::encryptString($emp['username']);

            // returning respond
            return response()->json([
                "success" => true,
                "error" => false,
                "not_confirmed"=>false,
                "message" => "You successfully authenticated!",
                'data' => [
                    "token_type" => "Bearer",
                    "expires_in" => $expiration,
                    "access_token" => $token,
                    "access" => $access,
                    "role"=>$emp['role']
                ]
                ], 200);
                
            }
        return response()->json([
            "success" => false,
            "error" => true,
            'message' => 'Пошта немесе құпия сөз дұрыс емес!',
            'data' => []
        ], 401);
       
    }

    public function getAdmins(){
        return response()->json([
            'success' => true,
            'error' => false,
            'message' => 'Successfully fregistered!',
            'data' => [
                'user'=>Employee::all()
            ]
        ], 200);
    }

    public function sendMessage(){
        $api = new Mobizon\MobizonApi('KKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK', 'api.mobizon.kz');
        return response()->json([
            'success' => true,
            'error' => false,
            'message' => 'Successfully registered!',
            'data' => [
                'user'=>'Employee::all()'
            ]
        ], 200);
    }

    public function sendOrder(Request $request){
        $validator =  Validator::make($request->all(),[
            'products'=>'required|array|min:1'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'error' => true,
                'message' =>'У вас в корзине нету товаров',
                'data' => [
                ]
            ], 200);
        }

        $validator =  Validator::make($request->all(),[
            'user.name'=>'required',
            'user.phone'=>'required',
            'user.address'=>'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'error' => true,
                'message' =>'Заполните все поля',
                'data' => [
                ]
            ], 200);
        }
        $user=$request->user;
        $products=$request->products;
        $order_book=OrderBooking::create([
            'fullname'=>$user['name'],
            'phone'=>$user['phone'],
            'address'=>$user['address']
        ]);
        foreach($products as $product){
            if($product['user_product_count']>0){
                OrderBookingProduct::create([
                    'product_id'=>$product['id'],
                    'order_booking_id'=>$order_book['id'],
                    'count'=>$product['user_product_count'],
                    'price'=>$product['price_discount']
                ]);
            }
        }
        
        return response()->json([
            'success' => true,
            'error' => false,
            'message' =>'',
            'data' => [
            ]
        ], 200);
    }
    
    public function sendPost(Request $request){
        $message_mail=$request->message;
        $to_name = 'Yedige';
        $to_email = 'yedigebaltabekov@gmail.com';
        $data = array('name'=>'Ogbonna Vitalis(sender_name)', 'body' => $message_mail);
        Mail::send('emails.message', $data, function($message) use ($to_name, $to_email) {
        $message->to($to_email, $to_name)
        ->subject('Заявка с сайта');
        $message->from('test@mail.ru','Test Mail');
        });
        return response()->json([
            'success' => true,
            'error' => false,
            'message' =>'',
            'data' => [
                'message'=>$request->message
            ]
        ], 200);
    }
    private function sendMail($mail, $code)
    {
        
    }
}
