<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderBookingProduct extends Model
{
    //
    protected $table = 'orderbookingproduct';

    protected $fillable = [
        'id',
        'product_id',
        'order_booking_id',
        'count',
        'price'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
