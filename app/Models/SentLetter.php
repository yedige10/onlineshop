<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SentLetter extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'recipient_mail'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];
}
