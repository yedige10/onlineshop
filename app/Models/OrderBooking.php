<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderBooking extends Model
{
    //
    protected $table = 'orderbooking';

    protected $fillable = [
        'id',
        'fullname',
        'phone',
        'address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
