<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Config;

class MessageMail extends Mailable
{
    use Queueable, SerializesModels;
    public $role;
    public $code, $from_mail;
    public $limit;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($code, $from_mail)
    {
        //
        $this->message = $message;
        $this->from_mail = $from_mail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'Завершение регистрации на сайте edutest.kz';

        $link = \URL::to('/').'/register/confirm/'.$this->code;

        return $this->text('mails.verification')
            ->from($this->from_mail)
            ->subject($subject);
    }
}