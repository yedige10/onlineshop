
window.Vue = require('vue');
import Router from './router/index.js'
import User from './App.vue'
import VueResource from 'vue-resource'
import Auth from './packages/auth/Auth.js'
import VModal from 'vue-js-modal'
Vue.use(VModal)
Vue.use(Auth)
Vue.use(VueResource)
Vue.http.headers.common['Content-Type'] = 'application/json'
Vue.http.headers.common['Authorization'] = Vue.auth.getToken()

export const cartBus = new Vue()

const app = new Vue({
    el: '#app',
    render: h => h(User),
    router : Router,
});