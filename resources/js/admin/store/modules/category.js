import Vue from 'vue'
import Alert from '../../packages/alert.js'
export default{
    actions:{
        newCategory({commit,dispatch},params){
            Vue.http.post('/api/admin/category',{category:params.category}).then(
                response=>{
                    if(response.body.success){
                        Alert.success('Категория успешно добавлена')
                        commit('updateCategory','')
                        dispatch('getCategories')
                    }else{
                        Alert.warning(response.body.message)
                    }
                }
            )
        },
        getCategories(ctx){
            Vue.http.get('/api/admin/category').then(
                response=>{
                    if(response.body.success){
                        ctx.commit('getCategories',response.body.data.categories)
                    }else{
                        Alert.warning(response.body.message)
                    }
                }
            )
        },
        deleteCategory({commit,dispatch},params){
            Vue.http.post('/api/admin/delete-category',{category:params.category}).then(
                response=>{
                    if(response.body.success){
                        Alert.success(response.body.message)
                        dispatch('getCategories')
                        
                    }else{
                        Alert.warning(response.body.message)
                    }
                }
            )
        },
        editCategory({commit,dispatch},params){
            Vue.http.post('/api/admin/update-category',{category:params.category}).then(
                response=>{
                    if(response.body.success){
                        Alert.success('Категория успешно изменена')
                        dispatch('getCategories')
                        
                    }else{
                        Alert.warning(response.body.message)
                    }
                }
            )
        }
    },
    mutations:{
        updateCategory(state,category){
            state.category=category
        },
        getCategories(state,categories){
            state.categories=categories
        }
    },
    state:{
        category:'',
        categories:[]
    },
    getters:{
        getCategory(state){
            return state.category
        },
        getCategories(state){
            return state.categories
        }
    }
}