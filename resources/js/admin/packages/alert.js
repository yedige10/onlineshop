const Swal = require('sweetalert2')
import Vue from 'vue'
export default {
        success(message){
            Swal.fire({
                title: '',
                icon:'success',
                text: message,
                type: 'success',
                confirmButtonText: 'ОК'
            })
        },
        warning(message){
            Swal.fire({
                title: '',
                text: message,
                type: 'warning',
                confirmButtonText: 'ОК'
            })
        },
        error(message){
            Swal.fire({
                title: Vue.i18n.translate('alerts.error'),
                text: message,
                type: 'error',
                confirmButtonText: Vue.i18n.translate('alerts.close')
            })
        },
        deleteSure(message){
            Swal.fire({
                title: message,
                showCancelButton: true,
                confirmButtonText: `Да`,
                cancelButtonText: `Нет`,
              }).then((result) => {
                if (result.isConfirmed) {
                  return 1
                } else if (result.isDenied) {
                  return 0
                }
              })
        }

    }
