/* eslint-disable */
import router from '../router/index'
export default function(Vue){
    
    Vue.auth = {
        login(access_token, expires_in, tokent_type, userid){
            this.setToken(access_token, expires_in, tokent_type, userid)
        },
        emplogin(access_token, expires_in, tokent_type, role, user, access){
            localStorage.setItem('access_token', access_token)
            localStorage.setItem('expires_in', expires_in)
            localStorage.setItem('tokent_type', tokent_type)
            localStorage.setItem('role', role)
            //localStorage.setItem('user', user)
            localStorage.setItem('access', access)
        },
        setToken(access_token, expires_in, tokent_type, user){
            localStorage.setItem('access_token', access_token)
            localStorage.setItem('expires_in', expires_in)
            localStorage.setItem('tokent_type', tokent_type)
        },
        //get token
        getToken(){
            var token = localStorage.getItem('access_token')
            var expiration = localStorage.getItem('expires_in')
            var typeis =  localStorage.getItem('tokent_type')
            var user=localStorage.getItem('user')
            
            if( !token || !expiration)
                return null
            
            
            return typeis + ' ' + token
            
        },
        getAccess(){
            return localStorage.getItem('access')
        },
        //get user
        getUser(){
            return localStorage.getItem('user')
        },
        setUser(user){
            localStorage.setItem('user', JSON.stringify(user))
        },
        getUserId(){
            var obj = JSON.parse(localStorage.getItem('user'));
            return obj.employee_id;
        },
        //destroy token
        destroyToken(){
            router.push({path:'/'})
            localStorage.clear();
        },
        //is Authenticated
        isAuthenticated(){
            if(this.getToken() != null)
                return true
            else
                return false
        },
        checkRole(role){
            return  role == localStorage.getItem('role') ? true : false
        },
        getPathTo(){
            if(localStorage.getItem('role') == 'admin'){
                return '/admin/category'
            }
            else if(localStorage.getItem('role') == 2){
                return '/moderator'
            }
            if(localStorage.getItem('role') == 3){
                return '/editor'
            }else{
                return '/'
            }
        }
    }

    Object.defineProperties(Vue.prototype, {
        $auth:{
            get(){
                return Vue.auth
            }
        }
    })
}