window.Vue = require('vue');
import Router from './router/index.js'
import Admin from './App.vue'
import VueResource from 'vue-resource'
import Auth from './packages/Auth.js'
import store from './store'
import Alerts from './packages/alert.js'
import VModal from 'vue-js-modal'
import VueSweetalert2 from 'vue-sweetalert2';
import Vue from 'vue'
import VueEasyLightbox from 'vue-easy-lightbox'

import { ClientTable } from "vue-tables-2";

// Method 1. via Vue.use
Vue.use(VueEasyLightbox)

// Method 2. Register via Vue.component
Vue.component(VueEasyLightbox.name, VueEasyLightbox)

Vue.use(VueSweetalert2, {
    confirmButtonColor: '#41b882',
    cancelButtonColor: '#ff7674'
}) 
Vue.use(Auth)
Vue.use(VueResource)
Vue.use(Alerts)
Vue.use(VModal)
Vue.use(ClientTable);
Vue.http.headers.common['Content-Type'] = 'application/json'
Vue.http.headers.common['Authorization'] = Vue.auth.getToken()
const app = new Vue({
    el: '#app',
    store,
    render: h => h(Admin),
    router : Router,
})