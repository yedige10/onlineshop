import Vue from 'vue'
import VueRouter from 'vue-router'
import Category from '../views/category.vue'
import Product from '../views/product.vue'
import ListProducts from '../views/listProducts.vue'
import editProduct from '../components/product/editProduct.vue'
import Bookings from '../views/bookings.vue'
Vue.use(VueRouter)
const router = new VueRouter({
	mode: 'history',
	base: '/admin/',
	routes:[
		/*
		{
			path: "/",
			name: 'app',
			component: App,
			meta : {
				title : 'App page',
			}
		},
		*/
        {
			path: "/category",
			name: 'category',
			component: Category,
			meta : {
				title : 'Category page',
			}
		},
		{
			path: "/product",
			name: 'product',
			component: Product,
			meta : {
				title : 'Product page',
			}
		},
		{
			path: "/bookings",
			name: 'bookings',
			component: Bookings,
			meta : {
				title : 'Bookings page',
			}
		},
		{
			path: "/list-products/:id",
			name: 'listProducts',
			component: ListProducts,
			meta : {
				title : 'ListProducts page',
			}
		},
		{
			path: "/edit-products/:id",
			name: 'editProduct',
			component: editProduct,
			meta : {
				title : 'editProduct page',
			}
		},
        
	]
})

export default router
