import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Auth/login.vue'
import Main from '../views/main.vue'
import Product from '../views/product.vue'
import Cart from '../views/cart.vue'
Vue.use(VueRouter)
const router = new VueRouter({
	mode: 'history',
	routes:[
        {

            path: "/",
            name: 'main',
			component: Main,
			meta : {
				title : 'Main page',
			}
        },
		{

            path: "/product/:id",
            name: 'product',
			component: Product,
			meta : {
				title : 'Product page',
			}
        },
        {

            path: "/login",
            name: 'login',
			component: Login,
			meta : {
				title : 'Login page',
			}
        },
		{

            path: "/cart",
            name: 'cart',
			component: Cart,
			meta : {
				title : 'Cart page',
			}
        },
        
	]
})

export default router
