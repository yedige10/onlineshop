<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'EmployeeController@register');
Route::post('login', 'EmployeeController@login');
Route::post('sendMessage', 'EmployeeController@sendMessage');
Route::get('category','CategoryController@getCategories');
Route::get('product/{id}','ProductController@getProductsByCategory');
Route::post('send-order', 'EmployeeController@sendOrder');
Route::post('send-post', 'EmployeeController@sendPost');
Route::group(['middleware' => 'auth:employee-api'], function(){
    Route::prefix('/admin')->group(function () {
        Route::get('admins', 'EmployeeController@getAdmins');
        Route::post('category','CategoryController@createCategory');
        Route::get('category','CategoryController@getCategories');
        Route::post('delete-category','CategoryController@deleteCategory');
        Route::post('update-category','CategoryController@updateCategory');
        Route::post('product','ProductController@createProduct');
        Route::post('edit-product','ProductController@editProduct');
        Route::get('product-details/{id}','ProductController@getProduct');
        Route::get('product/{id}','ProductController@getProductsByCategory');
        Route::post('delete-product','ProductController@deleteProduct');
        Route::get('bookings','BookingController@getBookings');
    });    
});
